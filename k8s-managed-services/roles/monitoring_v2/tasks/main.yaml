---
- name: "{{ monitoring_internet_probe | ternary('I', 'Uni') }}nstall blackbox-exporter"
  kubernetes.core.helm:
    chart_ref: prometheus-community/prometheus-blackbox-exporter
    chart_version: "{{ monitoring_blackbox_version }}"
    release_namespace: "{{ monitoring_namespace }}"
    release_name: "kms-blackbox"
    release_state: "{{ monitoring_internet_probe | ternary('present', 'absent') }}"
    values: "{{ lookup('template', 'blackbox_exporter.yaml.j2') | from_yaml }}"
    wait: true
    update_repo_cache: true
  register: task_result
  until: task_result is not failed
  retries: "{{ network_error_retries }}"
  delay: "{{ network_error_delay }}"

- name: Expose Prometheus with a NodePort
  when: k8s_monitoring_enabled and k8s_global_monitoring_enabled
  tags:
  - ch-k8s-global-monitoring
  - ch-k8s-global-monitoring-nodeport
  kubernetes.core.k8s:
    apply: true
    definition: "{{ lookup('template', 'prometheus-nodeport.yaml') }}"
    validate:
      fail_on_error: true
      strict: true
  # Retry this task on failures
  register: k8s_apply
  until: k8s_apply is not failed
  retries: "{{ k8s_error_retries }}"
  delay: "{{ k8s_error_delay }}"

- name: Create architecture-specific service monitors
  block:
  - name: Create keepalived service monitor (frontend)
    kubernetes.core.k8s:
      state: present
      apply: true
      definition: "{{ lookup('template', 'keepalived-service-monitor.yaml.j2') }}"
      validate:
        fail_on_error: true
        strict: true
    # Retry this task on failures
    register: k8s_apply
    until: k8s_apply is not failed
    retries: "{{ k8s_error_retries }}"
    delay: "{{ k8s_error_delay }}"

  - name: Create bird service monitor (frontend)
    kubernetes.core.k8s:
      state: present
      apply: true
      definition: "{{ lookup('template', 'bird-service-monitor.yaml.j2') }}"
      validate:
        fail_on_error: true
        strict: true
    # Retry this task on failures
    register: k8s_apply
    until: k8s_apply is not failed
    retries: "{{ k8s_error_retries }}"
    delay: "{{ k8s_error_delay }}"

  - name: Create HAProxy service monitor (frontend)
    kubernetes.core.k8s:
      state: present
      apply: true
      definition: "{{ lookup('template', 'haproxy-service-monitor.yaml.j2') }}"
      validate:
        fail_on_error: true
        strict: true
    # Retry this task on failures
    register: k8s_apply
    until: k8s_apply is not failed
    retries: "{{ k8s_error_retries }}"
    delay: "{{ k8s_error_delay }}"

  - name: Create ch-k8s-LBaaS service monitors (frontend)
    kubernetes.core.k8s:
      definition: "{{ lookup('template', 'lbaas-service-monitor.yaml.j2') }}"
      apply: true
      validate:
        fail_on_error: true
        strict: true
    when: ch_k8s_lbaas_enabled
    # Retry this task on failures
    register: k8s_apply
    until: k8s_apply is not failed
    retries: "{{ k8s_error_retries }}"
    delay: "{{ k8s_error_delay }}"

  - name: Create rook-ceph-mgr service monitor
    kubernetes.core.k8s:
      state: present
      apply: true
      definition: "{{ lookup('template', 'rook-service-monitor.yaml.j2') }}"
      validate:
        fail_on_error: true
        strict: true
    when: k8s_storage_rook_enabled
    # Retry this task on failures
    register: k8s_apply
    until: k8s_apply is not failed
    retries: "{{ k8s_error_retries }}"
    delay: "{{ k8s_error_delay }}"

  - name: Create service monitor node-exporters (frontend)
    kubernetes.core.k8s:
      state: present
      apply: true
      definition: "{{ lookup('template', 'node-exporter-service-monitor.yaml') }}"
      validate:
        fail_on_error: true
        strict: true
    # Retry this task on failures
    register: k8s_apply
    until: k8s_apply is not failed
    retries: "{{ k8s_error_retries }}"
    delay: "{{ k8s_error_delay }}"
    tags:
    - node-exporter-service-monitor

  - name: Create service monitors for calico
    when: not image_versions.calico_use_tigera_operator
    kubernetes.core.k8s:
      state: present
      apply: true
      definition: "{{ lookup('template', 'calico-monitoring.yaml.j2') }}"
      validate:
        fail_on_error: true
        strict: true
    # Retry this task on failures
    register: k8s_apply
    until: k8s_apply is not failed
    retries: "{{ k8s_error_retries }}"
    delay: "{{ k8s_error_delay }}"
    tags:
    - calico-service-monitor

  - name: Create monitoring resources for calico (operator-based)
    when: image_versions.calico_use_tigera_operator
    kubernetes.core.k8s:
      state: present
      apply: true
      definition: "{{ lookup('template', 'calico-tigera-monitoring.yaml.j2') }}"
      validate:
        fail_on_error: true
        strict: true
    # Retry this task on failures
    register: k8s_apply
    until: k8s_apply is not failed
    retries: "{{ k8s_error_retries }}"
    delay: "{{ k8s_error_delay }}"
    tags:
    - calico-service-monitor

  - name: Create service monitor for etcd-backup
    kubernetes.core.k8s:
      state: present
      apply: true
      definition: "{{ lookup('template', 'etcd-backup.yaml.j2') }}"
      validate:
        fail_on_error: true
        strict: true
    when: etcd_backup_enabled | default(False) | bool
    tags:
    - etcd-backup-service-monitor

  - name: Create a pod monitor for fluxcd
    when: fluxcd_enabled
    kubernetes.core.k8s:
      state: present
      apply: true
      definition: "{{ lookup('template', 'fluxcd_podmonitor.yaml.j2') }}"
      validate:
        fail_on_error: true
        strict: true
    # Retry this task on failures
    register: k8s_apply
    until: k8s_apply is not failed
    retries: "{{ k8s_error_retries }}"
    delay: "{{ k8s_error_delay }}"
    tags:
    - fluxcd
...
