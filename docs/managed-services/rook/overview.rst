Rook
====

.. toctree::
   :maxdepth: 2
   :caption: Rook-based Ceph Cluster
   :hidden:

   General Information <general-information>
   removing-osds
   resizing-osds
   upgrades
   custom-storage


`Rook <https://rook.io/>`__ is an open-source , cloud-native storage for
Kubernetes.

.. todo::

   needs details
