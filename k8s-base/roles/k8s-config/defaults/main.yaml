---
k8s_network_pod_subnet: 10.244.0.0/16
k8s_network_service_subnet: 10.96.0.0/12
k8s_network_pod_subnet_v6: fdff:2::/56
# The service subnet is bounded; for 128-bit addresses, the mask must be >= 108
# The service cluster IP range is validated by the kube-apiserver to have at most 20 host bits
# https://github.com/kubernetes/kubernetes/blob/v1.9.2/cmd/kube-apiserver/app/options/validation.go#L29-L32
# https://github.com/kubernetes/kubernetes/pull/12841
k8s_network_service_subnet_v6: fdff:3::/108
k8s_network_bgp_worker_as: 64512
k8s_network_bgp_gateway_as: 65000
k8s_network_calico_ipipmode: "Never"
k8s_cri_url: 'unix:///var/run/containerd/containerd.sock'

supported_k8s_versions:
  - '1.24'
  - '1.25'
  - '1.26'

# Minor version of k8s and Calico, e.g., '1.18.9' becomes '1.18'.
k8s_version_minor: '{{ k8s_version | regex_replace("^v?(\d+.\d+)(.\d+)?$", "\1") }}'

calico_version_minor: '{{ image_version_map[k8s_version_minor].calico_version | regex_replace("^v?(\d+.\d+)(.\d+)?$", "\1") }}'

# Default image tags for 'our' services, separated by minor k8s release.
# To overwrite any of the tags you probably will have to copy the whole dict
# to your `config.toml` and make the necessary adjustments. Monitoring is
# is not included here because `kube-prometheus` comes with its own type
# of versioning.
image_version_map:
  "1.24":
    openstack:
      # https://github.com/kubernetes/cloud-provider-openstack/releases/tag/v1.26.0
      cinder_csi_plugin: "v1.26.0"
      openstack_cloud_controller_manager: "v1.26.0"
      # https://github.com/kubernetes-csi/external-attacher/releases/tag/v4.2.0
      csi_attacher: "v4.2.0"
      # https://github.com/kubernetes-csi/node-driver-registrar/releases/tag/v2.6.3
      csi_node_driver_registrar: "v2.6.3"
      # https://github.com/kubernetes-csi/external-provisioner/releases/tag/v3.4.0
      csi_provisioner: "v3.4.0"
      # https://github.com/kubernetes-csi/external-resizer/releases/tag/v1.7.0
      csi_resizer: "v1.7.0"
      # https://github.com/kubernetes-csi/external-snapshotter/releases/tag/v6.2.1
      csi_snapshotter: "v6.2.1"
      # https://github.com/kubernetes-csi/livenessprobe/releases/tag/v2.9.0
      csi_livenessprobe: "v2.9.0"
    calico_version: "{{ k8s_network_calico_custom_version | default('3.24.5') }}"
    calico_use_tigera_operator: "{{ k8s_network_calico_use_tigera_operator | default(False) }}"
    volume_snapshot_controller: "v6.2.1"

  "1.25":
    openstack:
      # https://github.com/kubernetes/cloud-provider-openstack/releases/tag/v1.27.1
      cinder_csi_plugin: "v1.27.1"
      openstack_cloud_controller_manager: "v1.27.1"
      # https://github.com/kubernetes-csi/external-attacher/releases/tag/v4.2.0
      csi_attacher: "v4.2.0"
      # https://github.com/kubernetes-csi/node-driver-registrar/releases/tag/v2.6.3
      csi_node_driver_registrar: "v2.6.3"
      # https://github.com/kubernetes-csi/external-provisioner/releases/tag/v3.4.1
      csi_provisioner: "v3.4.1"
      # https://github.com/kubernetes-csi/external-resizer/releases/tag/v1.7.0
      csi_resizer: "v1.7.0"
      # https://github.com/kubernetes-csi/external-snapshotter/releases/tag/v6.2.1
      csi_snapshotter: "v6.2.1"
      # https://github.com/kubernetes-csi/livenessprobe/releases/tag/v2.9.0
      csi_livenessprobe: "v2.9.0"
    calico_version: "{{ k8s_network_calico_custom_version | default('3.26.1') }}"
    calico_use_tigera_operator: "{{ k8s_network_calico_use_tigera_operator | default(True) }}"
    volume_snapshot_controller: "v6.2.1"

  "1.26":
    openstack:
      # https://github.com/kubernetes/cloud-provider-openstack/releases/tag/v1.27.1
      cinder_csi_plugin: "v1.27.1"
      openstack_cloud_controller_manager: "v1.27.1"
      # https://github.com/kubernetes-csi/external-attacher/releases/tag/v4.2.0
      csi_attacher: "v4.3.0"
      # https://github.com/kubernetes-csi/node-driver-registrar/releases/tag/v2.6.3
      csi_node_driver_registrar: "v2.8.0"
      # https://github.com/kubernetes-csi/external-provisioner/releases/tag/v3.4.1
      csi_provisioner: "v3.5.0"
      # https://github.com/kubernetes-csi/external-resizer/releases/tag/v1.7.0
      csi_resizer: "v1.8.0"
      # https://github.com/kubernetes-csi/external-snapshotter/releases/tag/v6.2.1
      csi_snapshotter: "v6.2.2"
      # https://github.com/kubernetes-csi/livenessprobe/releases/tag/v2.9.0
      csi_livenessprobe: "v2.10.0"
    calico_version: "{{ k8s_network_calico_custom_version | default('3.26.1') }}"
    calico_use_tigera_operator: true # enforce
    # https://kubernetes-csi.github.io/docs/snapshot-controller.html
    volume_snapshot_controller: "v6.2.1"

image_versions: "{{ image_version_map[k8s_version_minor] }}"
...
