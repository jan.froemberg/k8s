---
- name: Sanity checks (OpenStack)
  # If no config file is passed, then we infer the access information to the bucket via the sourced
  # OpenStack variables. If there is a config file given, then this role will *not* validate the credentials.
  # NOTE: In yaook/k8s, one cannot enable thanos if one is not in an OpenStack environment which exposes an
  # object storage via swift yet. That's a bug (https://gitlab.com/yaook/k8s/-/issues/356).
  when: (monitoring_thanos_objectstorage_config_file | length) == 0
  block:
  - name: Fail if OpenStack application credentials are used
    when: lookup('env', "OS_AUTH_TYPE") == "v3applicationcredential"
    fail:
      msg: >
        thanos does not support OpenStack application credentials for authenticating against
        a SWIFT endpoint yet. This task was added so you don't have to wonder and search through
        the logs if thanos (and prometheus as a consequence) don't come up properly. For more
        information take a look here (https://gitlab.com/yaook/k8s/-/issues/436#note_873556688).

        That leaves you with two options:

        1. Fall back to OpenStack `v3password` based authentication if you need thanos.
        2. Disable thanos (`use_thanos = false` in config.toml) if you don't need thanos.

        If things have changed in the meanwhile, please file an issue to the LCM and get rid
        of this error message :)

  - name: Are OpenStack credentials available?
    include_role:
      name: check-openstack-credentials

- name: Create namespace
  kubernetes.core.k8s:
    apply: true
    definition:
      apiVersion: v1
      kind: Namespace
      metadata:
        name: "{{ monitoring_namespace }}"
    validate:
      fail_on_error: true
      strict: true
  # Retry this task on failures
  register: k8s_apply
  until: k8s_apply is not failed
  retries: "{{ k8s_error_retries }}"
  delay: "{{ k8s_error_delay }}"
  tags:
  - monitoring
  - thanos

- name: Cleanup K8s resource for the thanos v1 deployment
  block:
  - name: Check if Thanos v1 is deployed
    kubernetes.core.k8s_info:
      kind: Pod
      api_version: v1
      namespace: "{{ monitoring_namespace }}"
      label_selectors:
      - app.kubernetes.io/version=v0.26.0
    register: k8s_get_thanos_v1
    until: k8s_get_thanos_v1 is not failed
    retries: "{{ k8s_error_retries }}"
    delay: "{{ k8s_error_delay }}"
    tags:
    - thanos

  - name: Include Thanos v1 cleanup tasks
    ansible.builtin.include_tasks: cleanup-thanos-v1.yaml
    when: k8s_get_thanos_v1.resources is defined and k8s_get_thanos_v1.resources | length != 0
    tags:
    - thanos

- name: Create thanos secret
  kubernetes.core.k8s:
    apply: true
    definition:
      apiVersion: v1
      type: Opaque
      kind: Secret
      metadata:
        name: "{{ monitoring_thanos_config_secret_name }}"
        namespace: "{{ monitoring_namespace }}"
      data:
        objstore.yml: "{{ lookup('template', 'thanos-objectstorage.yaml.j2') | b64encode }}"
    validate:
      fail_on_error: true
      strict: true
  # Retry this task on failures
  register: k8s_apply
  until: k8s_apply is not failed
  retries: "{{ k8s_error_retries }}"
  delay: "{{ k8s_error_delay }}"
  tags:
  - thanos

- name: "{{ monitoring_use_thanos | ternary('Add', 'Remove') }} bitnami Repo"
  kubernetes.core.helm_repository:
    name: bitnami
    repo_url: "{{ monitoring_thanos_helm_repo_url }}"
    repo_state: "{{ monitoring_use_thanos | ternary('present', 'absent') }}"
    force_update: true
  # Retry this task on failures
  register: task_result
  until: task_result is not failed
  retries: "{{ network_error_retries }}"
  delay: "{{ network_error_delay }}"
  tags:
  - thanos

- name: "{{ monitoring_use_thanos | ternary('I', 'Uni') }}nstall Thanos helm chart"
  kubernetes.core.helm:
    chart_ref: "{{ monitoring_thanos_chart_ref }}"
    chart_version: "{{ monitoring_thanos_chart_version }}"
    release_namespace: "{{ monitoring_namespace }}"
    release_name: "{{ monitoring_thanos_release_name }}"
    release_state: "{{ monitoring_use_thanos | ternary('present', 'absent') }}"
    values: "{{ lookup('template', 'thanos_values.yaml.j2') | from_yaml }}"
    wait: true
    update_repo_cache: true
  # Retry this task on failures
  register: task_result
  until: task_result is not failed
  retries: "{{ network_error_retries }}"
  delay: "{{ network_error_delay }}"
  tags:
  - thanos
...
