# Update k8s-rook from 1.6.* to 1.7.*
# https://rook.io/docs/rook/v1.7/ceph-upgrade.html#rook-operator-upgrade-process

---
# Verify that the Ceph cluster is healthy
- name: Include cluster health verification
  ansible.builtin.include_tasks: cluster_health_verification.yaml
- name: Check that mon volume storage class exists
  kubernetes.core.k8s_info:
    kind: StorageClass
    name: "{{ rook_mon_volume_storage_class }}"
  when: "rook_mon_volume"
  register: mon_storage_class
  failed_when: "not mon_storage_class.resources"
- name: Check that osd volume storage class exists
  kubernetes.core.k8s_info:
    kind: StorageClass
    name: "{{ rook_osd_storage_class }}"
  register: osd_storage_class
  failed_when: "not osd_storage_class.resources"
  when: rook_on_openstack
- name: Add annotations and labels for Helm to adopt existing resources
  block:
    - name: Get all rook CRDs of group ceph.rook.io  # noqa no-changed-when
      ansible.builtin.command: "kubectl api-resources --api-group=ceph.rook.io -o name"
      register: rook_crds

    - name: Add annotations and labels for Helm to rook CRDs of group ceph.rook.io
      kubernetes.core.k8s:
        definition:
          apiVersion: "apiextensions.k8s.io/v1"
          kind: "CustomResourceDefinition"
          metadata:
            name: "{{ item }}"
            annotations:
              "meta.helm.sh/release-namespace": "{{ rook_namespace }}"
              "meta.helm.sh/release-name": "{{ rook_helm_release_name_operator }}"
            labels:
              "app.kubernetes.io/managed-by": "Helm"
      loop: "{{ rook_crds.stdout_lines }}"

    - name: Get all rook CRDs of group rook.io  # noqa no-changed-when
      ansible.builtin.command: "kubectl api-resources --api-group=rook.io -o name"
      register: rook_crds

    - name: Add annotations and labels for Helm to rook CRDs of group rook.io
      kubernetes.core.k8s:
        definition:
          apiVersion: "apiextensions.k8s.io/v1"
          kind: "CustomResourceDefinition"
          metadata:
            name: "{{ item }}"
            annotations:
              "meta.helm.sh/release-namespace": "{{ rook_namespace }}"
              "meta.helm.sh/release-name": "{{ rook_helm_release_name_operator }}"
            labels:
              "app.kubernetes.io/managed-by": "Helm"
      loop: "{{ rook_crds.stdout_lines }}"

    - name: Get all rook CRDs of group objectbucket.io  # noqa no-changed-when
      ansible.builtin.command: "kubectl api-resources --api-group=objectbucket.io -o name"
      register: rook_crds

    - name: Add annotations and labels for Helm to rook CRDs of group objectbucket.io
      kubernetes.core.k8s:
        definition:
          apiVersion: "apiextensions.k8s.io/v1"
          kind: "CustomResourceDefinition"
          metadata:
            name: "{{ item }}"
            annotations:
              "meta.helm.sh/release-namespace": "{{ rook_namespace }}"
              "meta.helm.sh/release-name": "{{ rook_helm_release_name_operator }}"
            labels:
              "app.kubernetes.io/managed-by": "Helm"
      loop: "{{ rook_crds.stdout_lines }}"

    - name: Add annotations and labels for Helm to cluster wide resources
      kubernetes.core.k8s:
        definition:
          apiVersion: "{{ item.0 }}"
          kind: "{{ item.1 }}"
          metadata:
            name: "{{ item.2 }}"
            annotations:
              "meta.helm.sh/release-namespace": "{{ rook_namespace }}"
              "meta.helm.sh/release-name": "{{ rook_helm_release_name_operator }}"
            labels:
              "app.kubernetes.io/managed-by": "Helm"
      loop:
        - ["rbac.authorization.k8s.io/v1", "ClusterRole", "cephfs-csi-nodeplugin"]
        - ["rbac.authorization.k8s.io/v1", "ClusterRole", "cephfs-external-provisioner-runner"]
        - ["rbac.authorization.k8s.io/v1", "ClusterRole", "psp:rook"]
        - ["rbac.authorization.k8s.io/v1", "ClusterRole", "rbd-csi-nodeplugin"]
        - ["rbac.authorization.k8s.io/v1", "ClusterRole", "rbd-external-provisioner-runner"]
        - ["rbac.authorization.k8s.io/v1", "ClusterRole", "rook-ceph-cluster-mgmt"]
        - ["rbac.authorization.k8s.io/v1", "ClusterRole", "rook-ceph-global"]
        - ["rbac.authorization.k8s.io/v1", "ClusterRole", "rook-ceph-mgr-cluster"]
        - ["rbac.authorization.k8s.io/v1", "ClusterRole", "rook-ceph-mgr-system"]
        - ["rbac.authorization.k8s.io/v1", "ClusterRole", "rook-ceph-object-bucket"]
        - ["rbac.authorization.k8s.io/v1", "ClusterRole", "rook-ceph-osd"]
        - ["rbac.authorization.k8s.io/v1", "ClusterRole", "rook-ceph-system"]
        - ["rbac.authorization.k8s.io/v1", "ClusterRole", "rook-ceph-admission-controller-role"]
        - ["rbac.authorization.k8s.io/v1", "ClusterRoleBinding", "cephfs-csi-nodeplugin"]
        - ["rbac.authorization.k8s.io/v1", "ClusterRoleBinding", "cephfs-csi-provisioner-role"]
        - ["rbac.authorization.k8s.io/v1", "ClusterRoleBinding", "rbd-csi-nodeplugin"]
        - ["rbac.authorization.k8s.io/v1", "ClusterRoleBinding", "rbd-csi-provisioner-role"]
        - ["rbac.authorization.k8s.io/v1", "ClusterRoleBinding", "rook-ceph-global"]
        - ["rbac.authorization.k8s.io/v1", "ClusterRoleBinding", "rook-ceph-mgr-cluster"]
        - ["rbac.authorization.k8s.io/v1", "ClusterRoleBinding", "rook-ceph-object-bucket"]
        - ["rbac.authorization.k8s.io/v1", "ClusterRoleBinding", "rook-ceph-osd"]
        - ["rbac.authorization.k8s.io/v1", "ClusterRoleBinding", "rook-ceph-system-psp"]
        - ["rbac.authorization.k8s.io/v1", "ClusterRoleBinding", "rook-ceph-system"]
        - ["rbac.authorization.k8s.io/v1", "ClusterRoleBinding", "rook-csi-cephfs-plugin-sa-psp"]
        - ["rbac.authorization.k8s.io/v1", "ClusterRoleBinding", "rook-csi-cephfs-provisioner-sa-psp"]
        - ["rbac.authorization.k8s.io/v1", "ClusterRoleBinding", "rook-csi-rbd-plugin-sa-psp"]
        - ["rbac.authorization.k8s.io/v1", "ClusterRoleBinding", "rook-csi-rbd-provisioner-sa-psp"]
    - name: Add annotations and labels for Helm to namespaced resources
      kubernetes.core.k8s:
        definition:
          apiVersion: "{{ item.0 }}"
          kind: "{{ item.1 }}"
          metadata:
            namespace: "{{ rook_namespace }}"
            name: "{{ item.2 }}"
            annotations:
              "meta.helm.sh/release-namespace": "{{ rook_namespace }}"
              "meta.helm.sh/release-name": "{{ rook_helm_release_name_operator }}"
            labels:
              "app.kubernetes.io/managed-by": "Helm"
      loop:
        - ["v1", "ConfigMap", "rook-ceph-operator-config"]
        - ["apps/v1", "Deployment", "rook-ceph-operator"]
        - ["rbac.authorization.k8s.io/v1", "Role", "cephfs-external-provisioner-cfg"]
        - ["rbac.authorization.k8s.io/v1", "Role", "rbd-external-provisioner-cfg"]
        - ["rbac.authorization.k8s.io/v1", "Role", "rook-ceph-cmd-reporter"]
        - ["rbac.authorization.k8s.io/v1", "Role", "rook-ceph-mgr"]
        - ["rbac.authorization.k8s.io/v1", "Role", "rook-ceph-osd"]
        - ["rbac.authorization.k8s.io/v1", "Role", "rook-ceph-purge-osd"]
        - ["rbac.authorization.k8s.io/v1", "Role", "rook-ceph-system"]
        - ["rbac.authorization.k8s.io/v1", "RoleBinding", "cephfs-csi-provisioner-role-cfg"]
        - ["rbac.authorization.k8s.io/v1", "RoleBinding", "rbd-csi-provisioner-role-cfg"]
        - ["rbac.authorization.k8s.io/v1", "RoleBinding", "rook-ceph-cluster-mgmt"]
        - ["rbac.authorization.k8s.io/v1", "RoleBinding", "rook-ceph-cmd-reporter-psp"]
        - ["rbac.authorization.k8s.io/v1", "RoleBinding", "rook-ceph-cmd-reporter"]
        - ["rbac.authorization.k8s.io/v1", "RoleBinding", "rook-ceph-default-psp"]
        - ["rbac.authorization.k8s.io/v1", "RoleBinding", "rook-ceph-mgr-psp"]
        - ["rbac.authorization.k8s.io/v1", "RoleBinding", "rook-ceph-mgr-system"]
        - ["rbac.authorization.k8s.io/v1", "RoleBinding", "rook-ceph-mgr"]
        - ["rbac.authorization.k8s.io/v1", "RoleBinding", "rook-ceph-osd-psp"]
        - ["rbac.authorization.k8s.io/v1", "RoleBinding", "rook-ceph-osd"]
        - ["rbac.authorization.k8s.io/v1", "RoleBinding", "rook-ceph-purge-osd"]
        - ["rbac.authorization.k8s.io/v1", "RoleBinding", "rook-ceph-system"]
        - ["v1", "ServiceAccount", "rook-ceph-admission-controller"]
        - ["v1", "ServiceAccount", "rook-ceph-cmd-reporter"]
        - ["v1", "ServiceAccount", "rook-ceph-mgr"]
        - ["v1", "ServiceAccount", "rook-ceph-osd"]
        - ["v1", "ServiceAccount", "rook-ceph-purge-osd"]
        - ["v1", "ServiceAccount", "rook-ceph-system"]
        - ["v1", "ServiceAccount", "rook-csi-cephfs-plugin-sa"]
        - ["v1", "ServiceAccount", "rook-csi-cephfs-provisioner-sa"]
        - ["v1", "ServiceAccount", "rook-csi-rbd-plugin-sa"]
        - ["v1", "ServiceAccount", "rook-csi-rbd-provisioner-sa"]

- name: Apply operator chart
  ansible.builtin.include_tasks: apply_operator.yaml
- name: Wait for rook to become ready
  ansible.builtin.include_tasks: wait_for_rook_upgrade.yaml
  when: operator_status.changed
  tags:
    - skip_ansible_lint

# This seemingly redundant step is necessary because Helm sometimes does not correctly adopt all resources if not executed with "--force". However, we do not want to force, because that could cause deletion and re-creation of resources. After this step, Helm recognizes all resources as its children and will henceforth care for them
- name: Generate operator manifests
  kubernetes.core.helm_template:
    chart_ref: rook-ceph
    chart_repo_url: https://charts.rook.io/release/
    chart_version: "{{ rook_version }}"
    release_namespace: "{{ rook_namespace }}"
    release_name: "{{ rook_helm_release_name_operator }}"
    values: "{{ lookup('template', 'operator-values.yaml.j2') | from_yaml }}"
    include_crds: true
  register: operator_manifests
  debugger: on_skipped
- name: Apply operator manifests
  kubernetes.core.k8s:
    definition: "{{ operator_manifests.stdout }}"
    apply: true
    namespace: "{{ rook_namespace }}"
  register: operator_status
- name: Wait for rook to become ready
  ansible.builtin.include_tasks: wait_for_rook_upgrade.yaml
  when: operator_status.changed
  tags:
    - skip_ansible_lint

- name: Include cluster health verification
  ansible.builtin.include_tasks: cluster_health_verification.yaml
...
