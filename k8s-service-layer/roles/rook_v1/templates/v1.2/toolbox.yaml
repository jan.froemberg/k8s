apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ "%s-tools" | format(rook_cluster_name) | to_json }}
  namespace: "{{ rook_namespace }}"
  labels:
    app: {{ "%s-tools" | format(rook_cluster_name) | to_json }}
spec:
  replicas: 1
  selector:
    matchLabels:
      app: {{ "%s-tools" | format(rook_cluster_name) | to_json }}
  template:
    metadata:
      labels:
        app: {{ "%s-tools" | format(rook_cluster_name) | to_json }}
    spec:
      priorityClassName: system-cluster-critical
      dnsPolicy: ClusterFirstWithHostNet
{% if rook_scheduling_key %}
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchExpressions:
              - key: {{ rook_scheduling_key | to_json }}
                operator: Exists
      tolerations:
      - key: {{ rook_scheduling_key | to_json }}
        operator: Exists
{% endif %}
      containers:
      - name: {{ "%s-tools" | format(rook_cluster_name) | to_json }}
        resources: {}
        image: {{ "rook/ceph:%s" | format(rook_version) | to_json }}
        command: ["/tini"]
        args: ["-g", "--", "/usr/local/bin/toolbox.sh"]
        imagePullPolicy: IfNotPresent
        env:
          - name: ROOK_ADMIN_SECRET
            valueFrom:
              secretKeyRef:
                name: rook-ceph-mon
                key: admin-secret
        securityContext:
          privileged: true
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
        volumeMounts:
          - mountPath: /dev
            name: dev
          - mountPath: /sys/bus
            name: sysbus
          - mountPath: /lib/modules
            name: libmodules
          - name: mon-endpoint-volume
            mountPath: /etc/rook
      # if hostNetwork: false, the "rbd map" command hangs, see https://github.com/rook/rook/issues/2021
      hostNetwork: true
      volumes:
        - name: dev
          hostPath:
            path: /dev
            type: Directory
        - name: sysbus
          hostPath:
            path: /sys/bus
            type: Directory
        - name: libmodules
          hostPath:
            path: /lib/modules
            type: Directory
        - name: mon-endpoint-volume
          configMap:
            defaultMode: 420
            name: rook-ceph-mon-endpoints
            items:
            - key: data
              path: mon-endpoints
