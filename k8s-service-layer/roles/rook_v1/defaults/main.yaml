---
rook_manage_pod_budgets: true

rook_nmons: 3
rook_namespace: "rook-ceph"
rook_mon_volume: true
rook_mon_volume_storage_class: local-storage
rook_mon_volume_size: 10Gi
rook_mon_allow_multiple_per_node: false
rook_mon_memory_limit: "1Gi"
rook_mon_memory_request: "{{ rook_mon_memory_limit }}"
rook_mon_cpu_limit: "500m"
rook_mon_cpu_request: "100m"

rook_nmgrs: 1
rook_mgr_use_pg_autoscaler: true
rook_mgr_memory_limit: "512Mi"
rook_mgr_memory_request: "{{ rook_mgr_memory_limit }}"
rook_mgr_cpu_limit: "500m"
rook_mgr_cpu_request: "100m"

rook_nosds: 3
rook_osd_volume_size: 90Gi
rook_osd_storage_class: csi-sc-cinderplugin
rook_osd_anti_affinity: true
rook_osd_autodestroy_safe: true
rook_osd_memory_limit: "2Gi"
rook_osd_memory_request: "{{ rook_osd_memory_limit }}"
rook_osd_cpu_limit: "500m"
rook_osd_cpu_request: "{{ rook_osd_cpu_limit }}"
rook_encrypt_osds: false

rook_use_all_available_devices: true
rook_use_all_available_nodes: true
rook_on_openstack: true

rook_mds_memory_limit: "4Gi"
rook_mds_memory_request: "{{ rook_mds_memory_limit }}"
rook_mds_cpu_limit: "1"
rook_mds_cpu_request: "{{ rook_mds_cpu_limit }}"

rook_operator_memory_limit: "512Mi"
rook_operator_memory_request: "{{ rook_operator_memory_limit }}"
rook_operator_cpu_limit: "1"
rook_operator_cpu_request: "{{ rook_operator_cpu_limit }}"

rook_csi_plugins: true

rook_toolbox: true

rook_ceph_fs: false
rook_ceph_fs_name: "ceph-fs"
rook_ceph_fs_replicated: 1
rook_ceph_fs_preserve_pools_on_delete: false

rook_pools:
- name: data
  create_storage_class: block
  replicated: 1

rook_nodeplugin_toleration: true

rook_use_host_networking: false

rook_version: "v1.7.11"  # default
rook_conf_major_version: "{{ rook_version.split('.')[0] }}"
rook_conf_minor_version: "{{ rook_version.split('.')[1] }}"
# v1.2.3 --> v1.2
rook_conf_maj_min_version: "{{ '%s.%s' | format(rook_conf_major_version, rook_conf_minor_version) }}"

rook_ceph_version_map:
  "v1.2.3":  # default
    ceph: "v14.2.6"  # default
  "v1.3.11":
    ceph: "v14.2.21"
  "v1.4.9":
    ceph: "v15.2.13"
  "v1.5.12":
    ceph: "v15.2.13"
  "v1.6.7":
    ceph: "v16.2.5"
  "v1.7.11":
    ceph: "v16.2.6"

rook_ceph_version: "{{ rook_ceph_version_map[rook_version]['ceph'] }}"

rook_scheduling_key: null
rook_mon_scheduling_key: null
rook_mgr_scheduling_key: null

# If OSDs are not replicated, the rook-ceph-operator will reject
# to perform upgrades, because OSDs will become unavailable.
# Set to True so rook will update even if OSDs would become unavailable
rook_skip_upgrade_checks: false

# Each rook release does only support certain major releases of ceph.
rook_ceph_release_map:
- rook_release: "v1.2"
  supported_ceph_releases:
  - v13
  - v14
- rook_release: "v1.3"
  supported_ceph_releases:
  - v14
  - v15
- rook_release: "v1.4"
  supported_ceph_releases:
  - v14
  - v15
- rook_release: "v1.5"
  supported_ceph_releases:
  - v14  # actually v14.2.5
  - v15  # actually v15.2.0
- rook_release: "v1.6"
  supported_ceph_releases:
  - v14
  - v15
  - v16
- rook_release: "v1.7"
  supported_ceph_releases:
  - v14  # >= 14.2.5
  - v15  # >= 15.2.0
  - v16  # >= 16.2.0

supported_rook_releases: "{{ rook_ceph_release_map | map(attribute='rook_release') }}"
supported_ceph_releases: "{{ rook_ceph_release_map | selectattr('rook_release', 'equalto', rook_conf_maj_min_version) | map(attribute='supported_ceph_releases') | flatten }}"
...
