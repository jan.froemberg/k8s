# Before moving on, the rook/ceph cluster’s core (RADOS) components
# (i.e., mons, mgrs, and OSDs) must be fully updated.
---
- name: Wait until rook upgrade completes
  block:
  - name: Set retry count
    ansible.builtin.set_fact:
      retry_count: "{{ 0 if retry_count is undefined else (retry_count | int) + 1 }}"

  - name: Gather information about rook deployments
    kubernetes.core.k8s_info:
      kind: Deployment
      namespace: "{{ rook_namespace }}"
      label_selectors:
      - rook_cluster = {{ rook_namespace }}
    register: rook_deployments_info

  - name: Validate that the new rook version is rolled out
    ansible.builtin.debug:
      msg: |
        Deployment {{ item.metadata.name }} has rook version {{ item.metadata.labels['rook-version'] }}
        Configured Replicas:  {{ item.status.replicas }}
        Updated Replicas:     {{ item.status.updatedReplicas }}
        Ready Replicas:       {{ item.status.readyReplicas }}
    failed_when: |
      ((item.status.replicas | int) != (item.status.updatedReplicas | int))
      or ((item.status.replicas | int) != (item.status.readyReplicas | int))
      or (not ((item.metadata.labels['rook-version']) is version(rook_version, operator='eq')))
    with_items: "{{ rook_deployments_info.resources }}"
    loop_control:
      label: "Deployment: {{ item.metadata.name }}"

  rescue:
  - name: Fail if the cluster did not converge
    ansible.builtin.fail:
      msg: "Rook components did not converge after the upgrade ({{ retry_count }} retries)"
    when: (retry_count | int) == 180

  - name: Pause for 20 seconds, give the cluster time to converge
    ansible.builtin.pause:
      seconds: 20

  # Recheck if retry_count limit has not been reached, yet
  - ansible.builtin.include_tasks: wait_for_rook_upgrade.yaml
...
